from django.urls import path
from django.urls.conf import include
from .views import index, friend_list


urlpatterns = [
    path('', index, name='index'),
    # Add friends path using friend_list Views
    path('friends', friend_list)
]
