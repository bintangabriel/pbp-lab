from django.http import request
from django.shortcuts import render
from.models import Note
from django.http.response import HttpResponse
from django.core import serializers

# Create your views here.
def index(request):
    notes = Note.objects.all()
    return render(request, "lab_2.html", context={
        'notes' : notes
    })

def xml(request):
    dt = serializers.serialize('xml', Note.objects.all())
    return HttpResponse(dt, content_type="application/xml")

def json(request):
    dt = serializers.serialize('json', Note.objects.all())
    return HttpResponse(dt, content_type="application/json")