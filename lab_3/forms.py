from django import forms
from django.db.models import fields
from django.forms import widgets

from lab_3.models import Friend

class DateInput(forms.DateInput):
    input_type = 'date'

class FriendForm(forms.ModelForm):
    class Meta:
        model = Friend
        fields = "__all__"
        error_messages = {
            'requred' : "Please fill the information"
        }
        input_att = {
            'type' : 'text'
        }
        widgets = {'dob' : DateInput}
        # dob = forms.DateField(widget=forms.DateInput({
        #     'type':'date'
        #     }))
        # display_name = forms.CharField(label='',required=False,
        # max_length=30, widget=forms.TextInput(attrs=input_att))
