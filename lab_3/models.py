from django import forms
from django.db import models
from django.db.models.fields import CharField

# Define friend model
class Friend(models.Model):
    name = models.CharField(max_length=30)
    npm = models.IntegerField()
    dob = models.DateField()