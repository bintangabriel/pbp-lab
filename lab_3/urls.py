from django.urls import path
from django.urls.conf import include
from .views import index, add_friend


urlpatterns = [
    path('', index, name='index'),
    # Add friends path using friend_list Views
    path('add', add_friend)
]
