from django.http.response import HttpResponse, HttpResponseRedirect
from django.shortcuts import render
from .forms import FriendForm
from .models import Friend
from django.contrib.auth import authenticate, login
from django.contrib.auth.decorators import login_required
# Create method to render page
@login_required(login_url='/admin/login/')
def index(request):
    friends = Friend.objects.all()
    return render(request, "lab3_index.html", context={
        'friends' : friends
    })
    
@login_required(login_url='/admin/login/')
def add_friend(request):
    form = FriendForm(request.POST or None)
    if (form.is_valid() and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-3')
    else:
        return render(request, "lab3_form.html", context={
            'form': form
        })
