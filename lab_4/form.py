from django import forms
from django.forms import fields
from .models import Note

class NoteForm(forms.ModelForm):
    class Meta:
        model = Note
        fields = "__all__"
        error_messages = {
            'required' : "Please fill the information"
        }
        input_att = {
            'type' : 'text'
        }