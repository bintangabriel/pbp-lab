from django.urls import path
from django.urls.conf import include
from .views import index, add_note, note_list


urlpatterns = [
    path('', index, name='index'),
    path('index', index),
    # Add friends path using friend_list Views
    path('add', add_note),
    path('note-list', note_list)
]
