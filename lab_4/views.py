from django.http.response import HttpResponseRedirect
from django.shortcuts import render
from .models import Note
from .form import NoteForm
# Create your views here.
def index(request):
    notes = Note.objects.all()
    return render(request, "lab4_index.html", context={
        'notes' : notes
    })

def add_note(request):
    form = NoteForm(request.POST or None)
    if (form.is_valid() and request.method == "POST"):
        form.save()
        return HttpResponseRedirect('/lab-4')
    else:
        return render(request, "lab4_form.html", context={
            'form' : form
        })

def note_list(request):
    notes = Note.objects.all()
    return render(request, "lab4_note_list.html", context={
        'notes' : notes
    })