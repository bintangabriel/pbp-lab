import 'package:flutter/material.dart';
import 'story.dart';

void main() => runApp(MyApp());
const primaryColor = Color(0xFF8DB580);

class MyApp extends StatelessWidget {
  Widget build(BuildContext context) {
    // const appName = 'ZonaHijau';
    return MaterialApp(
      title: 'teman-covid',
      color: Colors.teal[600],
      theme: ThemeData(
          primaryColor: primaryColor,
          brightness: Brightness.light,
          // primaryColor: Colors.teal[600],
          // fontFamily: 'Sora',
          textTheme: const TextTheme(
            headline2: TextStyle(
                fontSize: 17, fontWeight: FontWeight.bold, color: Colors.black),
            bodyText2: TextStyle(
                fontSize: 15,
                fontWeight: FontWeight.normal,
                color: Colors.grey),
            headline1: TextStyle(
                fontSize: 30, fontWeight: FontWeight.bold, color: Colors.black),
            bodyText1: TextStyle(
                fontSize: 25,
                fontWeight: FontWeight.normal,
                color: Colors.grey),
          )),
      home: const story(
        title: 'teman-covid',
      ),
    );
  }
}
