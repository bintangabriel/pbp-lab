import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:lab_6/main.dart';

class story extends StatelessWidget {
  final String title;
  const story({Key? key, required this.title}) : super(key: key);
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Container(
          padding: const EdgeInsets.only(top: 5, bottom: 5),
          child: Image.asset(
            "img/temancovid.png",
            height: MediaQuery.of(context).size.height / 20,
            fit: BoxFit.scaleDown,
          ),
        ),
        backgroundColor: primaryColor,
        elevation: 0,
      ),
      bottomNavigationBar: BottomNavigationBar(
        backgroundColor: primaryColor,
        elevation: 0,
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Teman Covid',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.medication),
            label: 'Informasi Obat',
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.explore),
              label: 'Pengalaman',
              backgroundColor: Colors.greenAccent),
          BottomNavigationBarItem(
            icon: Icon(Icons.wash),
            label: 'Wash Your Hand',
          ),
        ],
        currentIndex: 2,
      ),
      body: SingleChildScrollView(
        child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: const EdgeInsets.only(left: 30, top: 5.0),
                child: Center(
                  child: Text(
                    "OUR STORIES",
                    style: Theme.of(context).textTheme.headline1,
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top: 10.0),
                child: CarouselSlider(
                  options: CarouselOptions(
                    height: 300,
                    viewportFraction: 0.5,
                    autoPlay: true,
                    autoPlayInterval: const Duration(seconds: 4),
                    autoPlayAnimationDuration:
                        const Duration(milliseconds: 800),
                    autoPlayCurve: Curves.fastOutSlowIn,
                    enlargeCenterPage: true,
                    scrollDirection: Axis.horizontal,
                  ),
                  items: [
                    Container(
                      width: 250,
                      margin: const EdgeInsets.all(6.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        border: Border.all(color: Colors.black),
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 25.0, top: 25.0),
                              child: Text(
                                "By: Bintang",
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Flexible(
                                  child: Text(
                                    "Cruising when the sun goes down Across the sea Searching for something inside of me I would find all the lost pieces Hardly feel, deep and realI was blinded now I see",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                )),
                          ]),
                    ),
                    Container(
                      width: 250,
                      margin: const EdgeInsets.all(6.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        border: Border.all(color: Colors.black),
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 25.0, top: 25.0),
                              child: Text(
                                "By: Asha",
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Flexible(
                                  child: Text(
                                    "this tutorial, we will learn how to slugify urls in Django. A Slug is a short label for something, containing only letters, underscores or hyphens.",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                )),
                          ]),
                    ),
                    Container(
                      width: 250,
                      margin: const EdgeInsets.all(6.0),
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(8.0),
                        border: Border.all(color: Colors.black),
                      ),
                      child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding:
                                  const EdgeInsets.only(left: 25.0, top: 25.0),
                              child: Text(
                                "By: Yudha",
                                style: Theme.of(context).textTheme.headline2,
                              ),
                            ),
                            Padding(
                                padding: const EdgeInsets.all(15.0),
                                child: Flexible(
                                  child: Text(
                                    "Meet Django Django is a high-level Python web framework that encourages rapid development and clean, pragmatic design. Built by experienced developers, it takes care of much of the hassle of web ",
                                    style:
                                        Theme.of(context).textTheme.bodyText2,
                                  ),
                                )),
                          ]),
                    ),
                  ],
                ),
              ),
            ]),
      ),
      floatingActionButton: FloatingActionButton(
        onPressed: () {},
        child: const Icon(Icons.add),
        backgroundColor: primaryColor,
      ),
    );
  }
}
